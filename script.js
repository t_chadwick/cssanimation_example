// When the button is clicked
$('#btn1').on('click', function(){
  // Toggle the moving class on the ball
  $('.ball').toggleClass('moving');
});

$('#btn2').on('click', function(){
  // Toggle the moving class on the ball
  $('.ball').toggleClass('growing');
});

$('.ball').on('click', function(){
  $('.ball').toggleClass('paused');
});

$('#btn-bottom').on('click', function(){
  $('#cube').attr('class', '');
  $('#cube').toggleClass('show-bottom');
});
$('#btn-top').on('click', function(){
  $('#cube').attr('class', '');
  $('#cube').toggleClass('show-top');
});
$('#btn-left').on('click', function(){
  $('#cube').attr('class', '');
  $('#cube').toggleClass('show-left');
});
$('#btn-right').on('click', function(){
  $('#cube').attr('class', '');
  $('#cube').toggleClass('show-right');
});
$('#btn-front').on('click', function(){
  $('#cube').attr('class', '');
  $('#cube').toggleClass('show-front');
});
$('#btn-back').on('click', function(){
  $('#cube').attr('class', '');
  $('#cube').toggleClass('show-back');
});
